package com.archeinteractive.minigameapi;

import com.archeinteractive.minigameapi.config.BaseConfig;
import com.archeinteractive.minigameapi.world.MapManager;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

public abstract class Game<T extends BaseConfig> extends BukkitRunnable {

    private Plugin plugin;
    private boolean cancelled = false;
    private MapManager<T> mapManager;

    public Game(Plugin plugin, Class<T> configClass) {
        this.plugin = plugin;
        this.mapManager = new MapManager(configClass);
        this.runTaskTimer(plugin, 1, 1);
    }

    public abstract void run();

    @Override
    public void cancel() {
        if (this.cancelled) {
            return;
        }

        this.cancelled = true;
        Bukkit.getScheduler().cancelTask(getTaskId());
    }

    public Plugin getPlugin() {
        return plugin;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public MapManager getMapManager() {
        return mapManager;
    }
}
