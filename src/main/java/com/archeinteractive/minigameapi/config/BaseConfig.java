package com.archeinteractive.minigameapi.config;

public class BaseConfig extends JsonConfig {
    private String mapName = "n/a";
    private String mapAuthor = "n/a";

    public String getMapName() {
        return mapName;
    }

    public String getMapAuthor() {
        return mapAuthor;
    }
}
