package com.archeinteractive.minigameapi.world;

import com.archeinteractive.minigameapi.config.BaseConfig;
import com.archeinteractive.minigameapi.config.JsonConfig;

import java.io.File;

public class LoadedMap<T extends BaseConfig> implements GameMap {
    private File world;
    private Class<T> configClass;
    private T config;

    public LoadedMap(File world, Class<T> configClass) {
        this.world = world;
        this.configClass = configClass;
        this.loadConfig();
    }

    @Override
    public File getWorld() {
        return this.world;
    }

    @Override
    public String getMapName() {
        return this.config.getMapName();
    }

    @Override
    public String getMapAuthor() {
        return this.config.getMapAuthor();
    }

    public T getConfig() {
        return configClass.cast(config);
    }

    private void loadConfig() {
        this.config = JsonConfig.load(new File(world + File.separator + "map.json"), configClass);
    }
}
