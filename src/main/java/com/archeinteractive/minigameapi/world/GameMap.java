package com.archeinteractive.minigameapi.world;

import java.io.File;

public interface GameMap {
    public File getWorld();

    public String getMapName();

    public String getMapAuthor();
}
