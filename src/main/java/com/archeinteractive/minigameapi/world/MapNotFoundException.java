package com.archeinteractive.minigameapi.world;

public class MapNotFoundException extends Exception {
    public MapNotFoundException() {
        super("Unable to find any compatible maps.");
    }
}
