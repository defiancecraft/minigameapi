package com.archeinteractive.minigameapi.world;

import org.bukkit.World;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class MapLoadingCompleteEvent extends Event {
    private static final HandlerList handlers = new HandlerList();
    private LoadedMap map;
    private World world;

    public MapLoadingCompleteEvent(LoadedMap map, World world) {
        this.map = map;
        this.world = world;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public LoadedMap getMap() {
        return map;
    }

    public World getWorld() {
        return world;
    }
}