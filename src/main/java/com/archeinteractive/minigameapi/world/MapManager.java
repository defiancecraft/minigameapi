package com.archeinteractive.minigameapi.world;

import com.archeinteractive.minigameapi.config.BaseConfig;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.WorldCreator;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class MapManager<T extends BaseConfig> {
    private Class<T> configClass;
    private List<LoadedMap<T>> maps = new ArrayList<LoadedMap<T>>();
    private LoadedMap<T> current = null;

    public MapManager(Class<T> configClass) {
        this.configClass = configClass;
    }

    public CycleCompleteEvent cycle() throws MapNotFoundException{
        load();
        if (maps.size() == 0) {
            throw new MapNotFoundException();
        }

        LoadedMap previous = current;
        if (maps.contains(current) && maps.indexOf(current) != maps.size() - 1) {
            current = maps.get(maps.indexOf(current) + 1);
        } else {
            current = maps.get(0);
        }

        MapSelectedEvent mapSelectedEvent = new MapSelectedEvent(current);
        Bukkit.getPluginManager().callEvent(mapSelectedEvent);

        if (mapSelectedEvent.getMap() != null) {
            current = mapSelectedEvent.getMap();
        }

        init();
        if (current != null) {
            clean(previous);
        }

        CycleCompleteEvent cycleCompleteEvent = new CycleCompleteEvent(current);
        Bukkit.getPluginManager().callEvent(cycleCompleteEvent);
        return cycleCompleteEvent;
    }

    private void init() {
        prepareWorld();
    }

    private void init(LoadedMap<T> map) {
        if (current != null) {
            clean(current);
        }

        current = map;
        prepareWorld();
    }

    private void prepareWorld() {
        WorldCreator creator = new WorldCreator(current.getWorld().getName());
        Bukkit.getPluginManager().callEvent(new PreMapLoadEvent(current, creator));
        World world = Bukkit.createWorld(creator);
        world.setAutoSave(false);
        Bukkit.getPluginManager().callEvent(new PostMapLoadEvent(current, world));
    }

    private void clean(LoadedMap map) {
        if (Bukkit.getWorlds().contains(map.getWorld().getName())) {
            Bukkit.unloadWorld(map.getWorld().getName(), false);
        }
    }

    public void load() {
        File worldContainer = Bukkit.getWorldContainer();
        for (File file : worldContainer.listFiles()) {
            if (file.isDirectory()) {
                for (File f : file.listFiles()) {
                    if (f.getName().equals("map.json")) {
                        for (LoadedMap loadedMap : maps) {
                            if (loadedMap.getWorld() == file) {
                                continue;
                            }
                        }

                        maps.add(new LoadedMap<>(file, configClass));
                    }
                }
            }
        }
    }

    public Class<T> getConfigClass() {
        return configClass;
    }

    public List<LoadedMap<T>> getMaps() {
        load();
        return maps;
    }

    public LoadedMap<T> getCurrent() {
        return current;
    }
}
