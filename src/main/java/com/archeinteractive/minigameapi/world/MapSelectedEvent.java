package com.archeinteractive.minigameapi.world;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * An event called immediately after selecting
 * the next game map.
 */
public class MapSelectedEvent extends Event {
    private static final HandlerList handlers = new HandlerList();
    private LoadedMap map;

    public MapSelectedEvent(LoadedMap map) {
        this.map = map;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public LoadedMap getMap() {
        return map;
    }

    public void setMap(LoadedMap map) {
        this.map = map;
    }
}
