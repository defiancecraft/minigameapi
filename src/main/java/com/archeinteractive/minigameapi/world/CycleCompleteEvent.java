package com.archeinteractive.minigameapi.world;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class CycleCompleteEvent extends Event {
    private static final HandlerList handlers = new HandlerList();
    private LoadedMap map;
    private World world;

    public CycleCompleteEvent(LoadedMap map) {
        this.map = map;
        Bukkit.getWorlds().forEach((World world) -> {
            if (world.getName().equalsIgnoreCase(this.map.getWorld().getName())) {
                this.world = world;
            }
        });
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public LoadedMap getMap() {
        return map;
    }

    public World getWorld() {
        return world;
    }
}
